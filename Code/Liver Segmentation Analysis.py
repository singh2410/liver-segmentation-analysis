#!/usr/bin/env python
# coding: utf-8

# # Liver Segmentation Analysis
# #By- Aarush Kumar
# #Dated: October 07,2021

# In[1]:


from IPython.display import Image
Image(url='https://radiologyassistant.nl/assets/anatomy-of-the-liver-segments/a5611447cc9801_4.jpg')


# In[2]:


get_ipython().system('pip install nibabel')


# In[3]:


import pandas as pd
import os;
import numpy as np;
import nibabel as nib;
import cv2;
import matplotlib.pyplot as plt


# In[4]:


label = nib.load('/home/aarush100616/Downloads/Projects/Liver Segmentation /Data/Bn sao ca segmentation-1.nii')
data = nib.load('/home/aarush100616/Downloads/Projects/Liver Segmentation /Data/Bn sao ca volume-1.nii')


# In[5]:


data_np = data.get_fdata()
label_np = label.get_fdata()


# In[6]:


# plot histogram with 255 bins
b, bins, patches = plt.hist(data_np[:,:,0],255)
plt.xlim([0,255])
plt.show()


# In[7]:


plt.plot()


# In[8]:


n_slice = np.random.randint(0, high=data_np.shape[2])
plt.figure();
plt.subplot(1,2,1)
plt.imshow(data_np[:,:,56])
plt.subplot(1,2,2);
plt.imshow(label_np[:,:,56]);


# In[9]:


n_slice = np.random.randint(0, high=data_np.shape[2])
plt.figure();
plt.subplot(1,2,1)
plt.imshow(data_np[50:400,60:410,n_slice], cmap='gray',clim=(0,500))
plt.subplot(1,2,2);
plt.imshow(label_np[50:400,60:410,n_slice]);


# In[10]:


for i in range(0,data_np.shape[2]):
    print(f'Slide {i}')
    plt.imshow(data_np[:,:,i],cmap='gray',clim=(0,500))
    plt.show()
    plt.imshow(label_np[:,:,i])
    plt.show()
    print(f'Value in segmentation {np.unique(label_np[:,:,i])}')


# In[11]:


val, count = np.unique(label_np,return_counts = True)
val,count


# In[12]:


1 - ((count[0])/np.sum(count))

